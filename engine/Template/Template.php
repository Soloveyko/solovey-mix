<?php
/**
 * | -----------------------------
 * | Created by expexes on 9/28/18 3:42 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | Template.php
 * | ---
 */

namespace Engine\Template;


interface Template
{
	function make($t, $v = []);
}