<?php
/**
 * | -----------------------------
 * | Created by expexes on 9/28/18 3:42 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | BladeTemplate.php
 * | ---
 */

namespace Engine\Template;


use Xiaoler\Blade\Compilers\BladeCompiler;
use Xiaoler\Blade\Engines\CompilerEngine;
use Xiaoler\Blade\Engines\EngineResolver;
use Xiaoler\Blade\Factory;
use Xiaoler\Blade\Filesystem;
use Xiaoler\Blade\FileViewFinder;

class BladeTemplate implements Template
{
	public $path = [__DIR__];
	public $cachePath = __DIR__ . '/cache';

	public $fileSystem;
	public $compiler;

	public $resolver;
	public $factory;

	/**
	 * BladeTemplate constructor.
	 * @param array $path
	 * @param $cachePath
	 */
	public function __construct(array $path, $cachePath)
	{
		$this->path = $path;
		$this->cachePath = $cachePath;
		$this->fileSystem = new Filesystem();
		$this->compiler = new BladeCompiler($this->fileSystem, $this->cachePath);

		$this->resolver = new EngineResolver;
		$this->resolver->register('blade', function () {
			return new CompilerEngine($this->compiler);
		});

		$this->factory = new Factory($this->resolver, new FileViewFinder($this->fileSystem, $this->path));
		$this->factory->addExtension('tpl', 'blade');
	}

	/**
	 * @param $t
	 * @param array $v
	 * @return string
	 * @throws \Throwable
	 */
	function make($t, $v = [])
	{
		return $this->factory->make($t, $v)->render();
	}
}