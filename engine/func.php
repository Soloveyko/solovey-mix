<?php

function getMimeType($filename)
{
	$idx = explode('.', $filename);
	$count_explode = count($idx);
	$idx = strtolower($idx[$count_explode - 1]);

	$mimet = array(
		'txt' => 'text/plain',
		'htm' => 'text/html',
		'html' => 'text/html',
		'php' => 'text/html',
		'css' => 'text/css',
		'js' => 'application/javascript',
		'json' => 'application/json',
		'xml' => 'application/xml',
		'swf' => 'application/x-shockwave-flash',
		'flv' => 'video/x-flv',

		// images
		'png' => 'image/png',
		'jpe' => 'image/jpeg',
		'jpeg' => 'image/jpeg',
		'jpg' => 'image/jpeg',
		'gif' => 'image/gif',
		'bmp' => 'image/bmp',
		'ico' => 'image/vnd.microsoft.icon',
		'tiff' => 'image/tiff',
		'tif' => 'image/tiff',
		'svg' => 'image/svg+xml',
		'svgz' => 'image/svg+xml',

		// archives
		'zip' => 'application/zip',
		'rar' => 'application/x-rar-compressed',
		'exe' => 'application/x-msdownload',
		'msi' => 'application/x-msdownload',
		'cab' => 'application/vnd.ms-cab-compressed',

		// audio/video
		'mp3' => 'audio/mpeg',
		'qt' => 'video/quicktime',
		'mov' => 'video/quicktime',

		// adobe
		'pdf' => 'application/pdf',
		'psd' => 'image/vnd.adobe.photoshop',
		'ai' => 'application/postscript',
		'eps' => 'application/postscript',
		'ps' => 'application/postscript',

		// ms office
		'doc' => 'application/msword',
		'rtf' => 'application/rtf',
		'xls' => 'application/vnd.ms-excel',
		'ppt' => 'application/vnd.ms-powerpoint',
		'docx' => 'application/msword',
		'xlsx' => 'application/vnd.ms-excel',
		'pptx' => 'application/vnd.ms-powerpoint',


		// open office
		'odt' => 'application/vnd.oasis.opendocument.text',
		'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
	);

	if (isset($mimet[$idx])) {
		return $mimet[$idx];
	} else {
		return 'application/octet-stream';
	}
}

function o2a($object)
{
	$a = [];
	$reflection = new \ReflectionClass($object);
	do {
		foreach ($reflection->getProperties() as $property) {
			$property->setAccessible(true);
			$a[$property->getName()] = $property->getValue($object);
		}
	} while ($reflection = $reflection->getParentClass());
	return $a;
}

function r(...$s)
{
	echo('<pre>');
	foreach ($s as $item) {
		var_dump($item);
	}
	echo('</pre>');
}

function startsWith($haystack, $needle)
{
	$length = strlen($needle);
	return (substr($haystack, 0, $length) === $needle);
}

function endsWith($haystack, $needle)
{
	$length = strlen($needle);

	return $length === 0 || (substr($haystack, -$length) === $needle);
}

function success($response, $code = 200)
{
	http_response_code($code);
	response([
		'ok' => true,
		'response' => $response
	]);
	die();
}

function error($reason = 'something wrong', $code = 500)
{
	http_response_code($code);
	response([
		'ok' => false,
		'reason' => $reason
	]);
	die();
}

function response(array $response)
{
	header('Content-Type: application/json');
	print json_encode($response);
}

function asset($path)
{
	return '/static/' . $path;
}

function cout($t)
{
	$out = fopen('php://stdout', 'w');
	fputs($out, $t);
	fclose($out);
}

function i($p)
{
	if (!isset($p) || empty($p))
		error('Invalid data!');

	return $p;
}

function iopt($p, $d = null)
{
	if (!isset($p) || empty($p))
		return $d;

	return $p;
}


function config()
{
	return $GLOBALS['config'];
}

/**
 * @return SoloveyRouter\Router
 */
function router()
{
	return $GLOBALS['router'];
}

/**
 * @return \Pixie\QueryBuilder\QueryBuilderHandler
 */
function db()
{
	return $GLOBALS['database_connection'] ?
		new \Pixie\QueryBuilder\QueryBuilderHandler($GLOBALS['database_connection']) :
		new \Pixie\QueryBuilder\QueryBuilderHandler($GLOBALS['database_connection'] = new \Pixie\Connection(config()['db']['driver'], config()['db']));
}

/**
 * @return \Delight\Auth\Auth|mixed
 */
function auth()
{
	return $GLOBALS['auth'] ?
		$GLOBALS['auth'] :
		$GLOBALS['auth'] = new \Delight\Auth\Auth(db()->pdo(), null, config()['db']['prefix']);
}

/**
 * @param $p
 * @param $t
 * @param array $v
 * @throws Throwable
 */
function renderBlade($p, $t, $v = [])
{
	print((new \Engine\Template\BladeTemplate([root() . '/' . $p . '/views'], root() . '/storage/' . $p . '/cache/blade'))->make($t, $v));
}