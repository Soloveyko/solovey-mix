<?php

// ref: https://gist.github.com/FreekPaans/03edc11657979fc2cd7bc4cc7a03306d

define('__CSRF_TOKEN_COOKIE_NAME', "csrf-token");
define('__CSRF_TOKEN_INPUT_NAME', "csrf-token");

function csrf_token()
{
	return get_or_create_token();
}

function csrf_name()
{
	return __CSRF_TOKEN_INPUT_NAME;
}

function generate_csrf_token()
{
	//from http://stackoverflow.com/a/31683058/345910
	$token = base64_encode(openssl_random_pseudo_bytes(32));
	return $token;
}

function get_or_create_token()
{
	if (isset($_COOKIE[__CSRF_TOKEN_COOKIE_NAME])) {
		return $_COOKIE[__CSRF_TOKEN_COOKIE_NAME];
	}
	$token = generate_csrf_token();
	setcookie(__CSRF_TOKEN_COOKIE_NAME, $token);
	return $token;
}

function csrf_input_field()
{
	return sprintf('<input name="%s" value="%s" type="hidden">', __CSRF_TOKEN_INPUT_NAME, get_or_create_token());
}

function csrf_meta_field()
{
	return sprintf('<meta name="%s" content="%s">', __CSRF_TOKEN_INPUT_NAME, get_or_create_token());
}

function verify_csrf_token()
{
	if (!isset($_COOKIE[__CSRF_TOKEN_COOKIE_NAME]))
		error("csrf cookie not present", 400);

//	if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
	if (!isset($_REQUEST[__CSRF_TOKEN_INPUT_NAME]))
		error("csrf token not present in request", 400);

	if ($_COOKIE[__CSRF_TOKEN_COOKIE_NAME] !== $_REQUEST[__CSRF_TOKEN_INPUT_NAME])
		error("invalid csrf token", 400);
//	}
}