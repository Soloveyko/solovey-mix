<?php


require_once __DIR__ . "/init.php";
require_once __DIR__ . "/csrf.php";

if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
	verify_csrf_token();
}

$router = new \SoloveyRouter\Router();

$admin_path = isset(config()['engine']['admin_path']) ? trim(config()['engine']['admin_path'], "/") : 'admin';
if (startsWith($_SERVER["REQUEST_URI"], "/$admin_path/") || $_SERVER["REQUEST_URI"] == "/$admin_path") {
	$router->group("/$admin_path/", function (\SoloveyRouter\Router $router) {
		require_once root() . "/admin/start.php";
	})->middleware(\Admin\Middlewares\IsAdminMiddleware::class);
} else if (startsWith($_SERVER['REQUEST_URI'], '/static/')) {
	header("Content-type: " . getMimeType($_SERVER['DOCUMENT_ROOT'] . $_SERVER['REQUEST_URI']));
	readfile($_SERVER['SCRIPT_FILENAME']);
	die();
} else {
	require_once root() . "/app/start.php";
}

try {
	$router->go($_SERVER['REQUEST_URI'], $_SERVER['REQUEST_METHOD']);
} catch (\SoloveyRouter\Exception\RouterException $e) {
	print $e->getMessage();
}