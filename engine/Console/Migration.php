<?php
/**
 * | -----------------------------
 * | Created by expexes on 9/28/18 11:04 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | Migration.php
 * | ---
 */

namespace Engine\Console;


use Engine\Scheme\Scheme;

class Migration
{
	private $models = [];

	/**
	 * @return array
	 */
	public function getModels(): array
	{
		return $this->models;
	}

	/**
	 * @param $model
	 * @return mixed
	 */
	public function model($model)
	{
		return $this->models[array_push($this->models, $model) - 1];
	}

	public function migrate()
	{
		foreach ($this->models as $model) {
			$scheme = new Scheme();
			/* @var Scheme $scheme */
			$scheme = $model::scheme($scheme);
			echo "- Migrating " . $model::table() . "..\n";
			echo " * Fields: " . implode(", ", array_keys($scheme->getScheme())) . "\n";
			if ($scheme->getScheme())
				db()->query($scheme->toSql($model::table()));
			if ($scheme->getAddition()) {
				foreach ($scheme->getAddition() as $index => $value) {
					echo " + addition #" . ($index + 1);
					echo " " . db()->pdo()->query($value)->execute() . "\n";
				}
			}
		}
	}
}