<?php
/**
 * | -----------------------------
 * | Created by expexes on 9/28/18 9:04 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | Scheme.php
 * | ---
 */

namespace Engine\Scheme;


class Scheme
{

	protected $scheme = [];

	protected $addition = [];

	/**
	 * @param $name
	 * @return Next
	 */
	public function increment($name)
	{
		$this->scheme[$name] = ['serial'];
		return (new Next($name, $this))->notNull();
	}

	/**
	 * @param $name
	 * @param int $size
	 * @return Next
	 */
	public function string($name, $size = 255)
	{
		$val = "VARCHAR" . ($size ? "($size)" : "");
		$this->scheme[$name] = [$val];
		return new Next($name, $this);
	}

	/**
	 * @param $name
	 * @param int $size
	 * @return Next
	 */
	public function text($name, $size = 0)
	{
		$val = "TEXT" . ($size ? "($size)" : "");
		$this->scheme[$name] = [$val];
		return new Next($name, $this);
	}

	/**
	 * @param $name
	 * @param int $size
	 * @return Next
	 */
	public function binary($name, $size = 0)
	{
		$val = "BINARY" . ($size ? "($size)" : "");
		$this->scheme[$name] = [$val];
		return new Next($name, $this);
	}

	/**
	 * @param $name
	 * @param int $size
	 * @return Next
	 */
	public function blob($name, $size = 0)
	{
		$val = "bytea" . ($size ? "($size)" : "");
		$this->scheme[$name] = [$val];
		return new Next($name, $this);
	}

	/**
	 * @param $name
	 * @return Next
	 */
	public function boolean($name)
	{
		$val = "BOOLEAN";
		$this->scheme[$name] = [$val];
		return new Next($name, $this);
	}

	/**
	 * @param $name
	 * @return Next
	 */
	public function date($name)
	{
		$val = "DATE";
		$this->scheme[$name] = [$val];
		return new Next($name, $this);
	}

	/**
	 * @param $name
	 * @return Next
	 */
	public function datetime($name)
	{
		$val = "DATETIME";
		$this->scheme[$name] = [$val];
		return new Next($name, $this);
	}

	/**
	 * @param $name
	 * @return Next
	 */
	public function decimal($name)
	{
		$val = "DECIMAL";
		$this->scheme[$name] = [$val];
		return new Next($name, $this);
	}

	/**
	 * @param $name
	 * @return Next
	 */
	public function float($name)
	{
		$val = "FLOAT";
		$this->scheme[$name] = [$val];
		return new Next($name, $this);
	}

	/**
	 * @param $name
	 * @return Next
	 */
	public function time($name)
	{
		$val = "TIME";
		$this->scheme[$name] = [$val];
		return new Next($name, $this);
	}

	/**
	 * @param $name
	 * @return Next
	 */
	public function timestamp($name)
	{
		$val = "TIMESTAMP";
		$this->scheme[$name] = [$val];
		return new Next($name, $this);
	}

	/**
	 * @param $name
	 * @param int $size
	 * @return Next
	 */
	public function varchar($name, $size = 255)
	{
		$val = "VARCHAR" . ($size ? "($size)" : "");
		$this->scheme[$name] = [$val];
		return new Next($name, $this);
	}

	/**
	 * @param $table
	 * @return string
	 */
	public function toSql($table)
	{
		$fields = $this->toSqlFields();
		return "
		CREATE TABLE \"$table\" (
			$fields
		) WITH (
		  OIDS=FALSE
		);
		";
	}

	/**
	 * @return bool|string
	 */
	public function toSqlFields()
	{
		$sql = "";
		foreach ($this->scheme as $i => $v) {
			$s = implode(" ", $v);
			$sql .= "\"${i}\" ${s},";
		}
		$sql = substr($sql, 0, (strlen($sql) > 0 ? strlen($sql) : 1) - 1);
		return $sql;
	}

	/**
	 * @param $n
	 * @param $v
	 * @return Next
	 */
	public function qset($n, $v)
	{
		array_push($this->scheme[$n], $v);
		return new Next($n, $this);
	}

	/**
	 * @param $sql
	 * @return $this
	 */
	public function sql($sql)
	{
		array_push($this->addition, $sql);
		return $this;
	}

	/**
	 * @return array
	 */
	public function getAddition()
	{
		return $this->addition;
	}

	/**
	 * @return array
	 */
	public function getScheme(): array
	{
		return $this->scheme;
	}
}