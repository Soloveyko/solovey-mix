<?php
/**
 * | -----------------------------
 * | Created by expexes on 9/28/18 11:00 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | Next.php
 * | ---
 */

namespace Engine\Scheme;


class Next
{
	protected $name;

	/** @var Scheme $scheme */
	protected $scheme;

	/**
	 * Next constructor.
	 * @param $name
	 * @param $scheme
	 */
	public function __construct($name, &$scheme)
	{
		$this->name = $name;
		$this->scheme = $scheme;
	}

	public function with($value)
	{
		$this->scheme->qset($this->name, $value);
		return $this;
	}

	public function notNull()
	{
		$this->scheme->qset($this->name, "NOT NULL");
		return $this;
	}

	public function unique()
	{
		$this->scheme->qset($this->name, "UNIQUE");
		return $this;
	}

	public function default($value)
	{
		$this->scheme->qset($this->name, "DEFAULT '$value'");
		return $this;
	}
}