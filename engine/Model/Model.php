<?php
/**
 * | -----------------------------
 * | Created by expexes on 9/28/18 8:37 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | Model.php
 * | ---
 */

namespace Engine\Model;


use Engine\Scheme\Scheme;

abstract class Model
{

	/**
	 * @param Model $model
	 * @return array|string
	 */
	static function create(Model $model)
	{
		$model = o2a($model);
		unset($model[static::id()]);
		return db()->table(static::table())->insert($model);
	}

	/**
	 * @return string
	 */
	static function id()
	{
		return 'id';
	}

	/**
	 * @return string
	 */
	static function table()
	{
		return 'table';
	}

	/**
	 * @param Model $model
	 * @return array|string
	 */
	static function update(Model $model)
	{
		$m = o2a($model);
		unset($m[static::id()]);
		if (db()->table(static::table())->where(static::id(), '=', $model->{static::id()})->update($m))
			return $model::getById($model->{static::id()});
		else
			return false;
	}

	/**
	 * @param $id
	 * @return null|Model
	 */
	static function getById($id)
	{
		return db()->from(static::table())->where(static::id(), '=', $id)->asObject(static::class)->first();
	}

	/**
	 * @param Model $model
	 * @return array|string
	 */
	static function save(Model $model)
	{
		$model = o2a($model);
		unset($model[static::id()]);
		return db()->table(static::table())->updateOrInsert($model);
	}

	/**
	 * @param $id
	 * @return mixed
	 */
	static function delete($id)
	{
		return db()->table(static::table())->where(static::id(), '=', $id)->delete();
	}

	/**
	 * @param $byName
	 * @param string $byOperator
	 * @param $byValue
	 * @return mixed
	 */
	static function getAllBy($byName, $byOperator = '=', $byValue)
	{
		return db()->from(static::table())->where($byName, $byOperator, $byValue)->asObject(static::class)->get();
	}

	/**
	 * @return mixed
	 */
	static function getAll()
	{
		return db()->from(static::table())->asObject(static::class)->get();
	}

	/**
	 * @return array
	 */
	static function required()
	{
		return [];
	}

	static function fields()
	{
		return array_keys(get_class_vars(static::class));
	}

	/**
	 * @return \Pixie\QueryBuilder\QueryBuilderHandler
	 */
	public static function sel()
	{
		return db()->table(static::table());
	}

	/**
	 * @param Scheme $scheme
	 * @return Scheme
	 */
	static function scheme(Scheme $scheme)
	{
		return $scheme;
	}

	public function setAll(array $a)
	{
		foreach ($a as $item => $value)
			$this->set($item, $value);
	}

	public function set($i, $v)
	{
		$this->{$i} = $v;
	}
}