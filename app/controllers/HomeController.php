<?php
/**
 * | -----------------------------
 * | Created by expexes on 9/28/18 3:47 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | HomeController.php
 * | ---
 */

namespace App\Controllers;


class HomeController
{

	public function index()
	{
		view('home');
	}
}