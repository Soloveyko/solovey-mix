<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>@yield('title', 'Application')</title>
	{!! csrf_meta_field() !!}

	<link rel="stylesheet" href="{{asset('app/app.css')}}">

	<script src="{{asset('app/app.js')}}" defer></script>
</head>
<body>
<div id="app">
	@yield('body', 'Hello World!')
</div>
</body>
</html>