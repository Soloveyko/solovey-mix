<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	{!! csrf_meta_field() !!}

	<title>@yield('title', 'Admin Application')</title>

	<link rel="stylesheet" href="{{asset('admin/admin.css')}}">

	<script src="{{asset('admin/admin.js')}}" defer></script>
</head>
<body>
<div id="app">
	@yield('body', 'Hello World at Admin Page!')
</div>
</body>
</html>