<?php
/**
 * | -----------------------------
 * | Created by expexes on 9/28/18 8:43 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | User.php
 * | ---
 */

namespace Admin\Models;


use Engine\Model\Model;
use Engine\Scheme\Scheme;

class User extends Model
{
	public $id;
	public $email;
	public $password;
	public $username;
	public $status;
	public $verified;
	public $resettable;
	public $roles_mask;
	public $registered;
	public $last_login;
	public $force_logout;
	public $name;


	public static function table()
	{
		return 'users';
	}

	public static function logged()
	{
		return static::getById(auth()->getUserId());
	}

	public static function scheme(Scheme $scheme)
	{
		$d = config()['db']['driver'];
		$scheme->sql(file_get_contents(root() . "/storage/database/schemas/auth_${d}_schema.sql"));

		return $scheme;
	}
}