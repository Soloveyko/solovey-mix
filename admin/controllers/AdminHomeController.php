<?php
/**
 * | -----------------------------
 * | Created by expexes on 9/28/18 3:47 PM.
 * | Site: teslex.tech
 * | ------------------------------
 * | AdminHomeController.php
 * | ---
 */

namespace Admin\Controllers;


class AdminHomeController
{

	function index()
	{
		view('home');
	}
}