<?php

function root()
{
	return __DIR__;
}

require_once __DIR__ . "/libs/vendor/autoload.php";
require_once __DIR__ . "/engine/bootstrap.php";